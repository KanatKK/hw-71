import { StatusBar } from 'expo-status-bar';
import React, {useReducer} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from "./styles/styles";

const initialState = {
  equations: '',
  error: 'none',
  calc: 'block',
};

const GET_EQUATIONS = 'GET_EQUATIONS';
const DELETE_SIGN = 'DELETE_SIGN';
const CLEAR_CALC = 'CLEAR_CALC';
const GET_DECISION = 'GET_DECISION';
const ERROR = 'ERROR';

const getError = () => {
  return {type: ERROR};
};

const getEquations = (value) => {
  return {type: GET_EQUATIONS, value};
};

const deleteLast = () => {
  return {type: DELETE_SIGN};
};

const clearCalc = () => {
  return {type: CLEAR_CALC};
};

const getDecision = (value) => {
  return {type: GET_DECISION, value};
};

const reducer = (state, action) => {
  switch (action.type) {
    case GET_EQUATIONS:
      return {
        ...state,
        equations: state.equations + action.value,
        error: 'none',
        calc: 'block',
      };
    case DELETE_SIGN:
      return {
        ...state,
        equations: state.equations.substring(0, state.equations.length - 1),
        error: 'none',
        calc: 'block',
      };
    case CLEAR_CALC:
      return {
        ...state,
        equations: '',
        error: 'none',
        calc: 'block',
      };
    case GET_DECISION:
      return {
        ...state,
        equations: action.value,
      };
    case ERROR:
      return {
        ...state,
        error: 'block',
        calc: 'none',
      }
    default:
      return state;
  }
};

export default function App() {
  const deleteSign = '<';

  const [state, dispatch] = useReducer(reducer, initialState);

  const addEquation = value => {
    dispatch(getEquations(value));
  };

  const deleteLastSign = () => {
    dispatch(deleteLast());
  };

  const calcClear = () => {
    dispatch(clearCalc());
  };

  const addDecision = () => {
    try {
      return eval(state.equations).toString();
    } catch(e) {
      return 'ERROR!';
    }
  };

  const decision = () => {
    if (addDecision() !== 'ERROR!') {
      dispatch(getDecision(addDecision()));
    } else {
      dispatch(getError());
    }
  };



  return (
    <View style={styles.container}>
      <View style={styles.calc}>
        <Text style={{
          fontSize: 30,
          color: 'red',
          display: state.error
        }}>ERROR!</Text>
        <Text style={{
          fontSize: 30,
          display: state.calc
        }}>{state.equations}</Text>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.deleteSigns} onPress={deleteLastSign}>
          <Text style={styles.font}>{deleteSign}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.deleteSigns} onPress={calcClear}>
          <Text style={styles.font}>CE</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('1')}>
          <Text style={styles.font}>1</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('2')}>
          <Text style={styles.font}>2</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('3')}>
          <Text style={styles.font}>3</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.signs} onPress={() => addEquation('+')}>
          <Text style={styles.font}>+</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('4')}>
          <Text style={styles.font}>4</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('5')}>
          <Text style={styles.font}>5</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('6')}>
          <Text style={styles.font}>6</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.signs} onPress={() => addEquation('-')}>
          <Text style={styles.font}>-</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('7')}>
          <Text style={styles.font}>7</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('8')}>
          <Text style={styles.font}>8</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('9')}>
          <Text style={styles.font}>9</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.signs} onPress={() => addEquation('*')}>
          <Text style={styles.font}>*</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('.')}>
          <Text style={styles.font}>.</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numbers} onPress={() => addEquation('0')}>
          <Text style={styles.font}>0</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.signs} onPress={decision}>
          <Text style={styles.font}>=</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.signs} onPress={() => addEquation('/')}>
          <Text style={styles.font}>/</Text>
        </TouchableOpacity>
      </View>
      <StatusBar style="auto" />
    </View>
  );
};


